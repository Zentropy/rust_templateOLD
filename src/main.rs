#[allow(unused_imports)]
use log::{error, info, warn, debug, trace};
use dotenv;

mod util;
#[allow(unused_imports)]
use util::string_utils::*;
#[allow(unused_imports)]
use util::file_utils::*;

fn main() {
    init();
}

fn init() {
    dotenv::dotenv().ok();
    init_logging();
    trace!("Initialization Complete");
}

fn init_logging() {    
    let my_subscriber = tracing_subscriber::fmt()
        .without_time()
        //.with_target(true)
        //.compact()
        .pretty()
        .finish();

    tracing::subscriber::set_global_default(my_subscriber).expect("Setting tracing default failed");
}




//////////////////////////TESTING

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn adds_properly() {
        assert_eq!(2+2, 4);
    }

    #[test]
    fn adds_improperly() {
        assert_ne!(2 + 2, 5);
    }
}
